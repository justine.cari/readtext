package ut.com.readtext;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

import com.readtext.manager.TxtReader;
import com.readtext.manager.TxtReaderImpl;
import com.readtext.model.TemplateFile;

public class TxtReaderTest {

	private TxtReader txtReader;

	@Before
	public void init() throws Exception {

		txtReader = new TxtReaderImpl();

	}

	@Test
	public void testReadFile() {

		File inputFile = new File("src/test/resources/utReadFile.txt");
		Exception exception = null;
		// Test
		TemplateFile file = null;
		try {
			file = txtReader.readFile(inputFile);
		} catch (Exception e) {
			exception = e;
		}

		// check inputFile
		assertEquals("utReadFile.txt", file.getInputFile());

		// check colors
		assertEquals('R', file.getReferences().get(0).getColor());
		assertEquals('B', file.getReferences().get(1).getColor());

		// check number
		assertEquals("1234567890", file.getReferences().get(0).getNumber());
		assertEquals("2345678901", file.getReferences().get(1).getNumber());

		// check price
		assertEquals(new BigDecimal("12.01"), file.getReferences().get(0).getPrice());
		assertEquals(new BigDecimal("143.07"), file.getReferences().get(1).getPrice());

		// check size
		assertEquals(1, file.getReferences().get(0).getSize());
		assertEquals(10, file.getReferences().get(1).getSize());
		
		//check errors
		assertEquals(exception, null);
	}

	@Test
	public void testReadFileWithWrongColor() {

		File inputFile = new File("src/test/resources/utReadFileWrongColor.txt");
		Exception exception = null;
				
		//Test
		TemplateFile file = null;
		try {
			file = txtReader.readFile(inputFile);
		} catch (Exception e) {
			exception = e;
		}
		
		//Checks that we have an error on the color
		assertEquals(1, file.getErrors().size());
		assertEquals("Incorrect value for color", file.getErrors().get(0).getMessage());
		
		//check errors
		assertEquals(exception, null);

	}

}
