package com.readtext.welcome;


import java.io.File;

import com.readtext.impl.model.UserInputsImpl;
import com.readtext.manager.FileGenerationManager;
import com.readtext.manager.FileGenerationManagerImpl;
import com.readtext.model.UserInputs;

public class Welcome {

	

	public static void main(String[] args) throws Exception {

		
		FileGenerationManager fileGenerationManager = new FileGenerationManagerImpl();
		
		//get the inputs of the user  
		UserInputs inputs = getUserInputs(args[0], args[1], args[2]);

		//generate the file
		fileGenerationManager.generateFile(inputs);

	}

	
	private static UserInputs getUserInputs(String txtFilePath, String fileType, String generatedFilePath) throws Exception {

		File txtFile = new File(txtFilePath);

		// checks if the file exists on computer
		if (!txtFile.exists()) {
			throw new Exception("The file " + txtFilePath + " does not exist.");
		}
		
		//checks that the type of file is either XML or JSON
		if (!fileType.equals("XML") && !fileType.equals("JSON")) {
			throw new Exception("Invalid value for file format.");
		}
		
		//format the generatedFilePath
		if (!generatedFilePath.endsWith("\\")){
			generatedFilePath=generatedFilePath+"\\";
		}
		
		// populate the object inputs with user entries
		UserInputs inputs = new UserInputsImpl(txtFile, fileType, generatedFilePath);
		
		return inputs;
	}

	

	

}
