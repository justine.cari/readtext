package com.readtext.manager;

import java.io.File;
import java.io.FileWriter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.readtext.model.TemplateFile;
import com.readtext.model.UserInputs;

public class FileGenerationManagerImpl implements FileGenerationManager {

	private static final String FILE_CREATION_ERROR = "Error while creating file";

	@Override
	public File generateFile(UserInputs inputs) throws Exception {

		TxtReader txtReader = new TxtReaderImpl();
				
		File inputFile = inputs.getTxtFile();

		//Reads the inputFile and create a template for the file to create
		TemplateFile templateFile = txtReader.readFile(inputFile);

		//creation of the file
		File outputFile =  createFile(inputs, templateFile);

		return outputFile;
	}

	private File createFile(UserInputs inputs, TemplateFile templateFile) {

		
		//creation of a JSON file if 
		switch (inputs.getType()) {
		
		case "JSON":
			//Creation of JSON file
			createJsonFile(inputs, templateFile);
			return new File(inputs.getGeneratedFilePath() + "GeneratedFile.json");
		default:
			//Creation of XML file by default
			createXmlFile(inputs, templateFile);
			return new File(inputs.getGeneratedFilePath() + "GeneratedFile.xml");
		}

	}

	private void createXmlFile(UserInputs inputs, TemplateFile templateFile) {
		String result = null;
		XmlMapper mapper = new XmlMapper();

		try {
			//Creation of the text for the file
			result = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(templateFile);
		} catch (JsonProcessingException e) {
			System.out.println(FILE_CREATION_ERROR);
		}

		try (FileWriter file = new FileWriter(inputs.getGeneratedFilePath() + "GeneratedFile.xml")) {
			//Writing in the file
			file.write(result);

		} catch (Exception e1) {

			System.out.println(FILE_CREATION_ERROR);
		}
		//information in console for users
		System.out.println("Generated file : " + inputs.getGeneratedFilePath() + "GeneratedFile.xml");
	}

	private void createJsonFile(UserInputs inputs, TemplateFile templateFile) {
		String result = null;
		ObjectMapper mapper = new ObjectMapper();

		try {
			//Creation of the text for the file
			result = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(templateFile);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		try (FileWriter file = new FileWriter(inputs.getGeneratedFilePath() + "GeneratedFile.json")) {
			try {
				//Writing in the file
				file.write(result);
			} catch (Exception e) {
				System.out.println(FILE_CREATION_ERROR);
			}

		} catch (Exception e1) {
			System.out.println(FILE_CREATION_ERROR);
		}
		//information in console for users
		System.out.println("Generated file : " + inputs.getGeneratedFilePath() + "GeneratedFile.json");
	}
}
