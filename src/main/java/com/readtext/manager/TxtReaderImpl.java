package com.readtext.manager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.readtext.impl.model.FileErrorImpl;
import com.readtext.impl.model.ReferenceImpl;
import com.readtext.impl.model.TemplateFileImpl;
import com.readtext.model.FileError;
import com.readtext.model.Reference;
import com.readtext.model.TemplateFile;

public class TxtReaderImpl implements TxtReader {

	@Override
	public TemplateFile readFile(File inputFile) throws Exception {

		// Create the references list for the future TemplateFile
		List<Reference> references = new ArrayList<>();
		// Create the errors list for the future TemplateFile
		List<FileError> errors = new ArrayList<>();

		// Create a reader for the inputFile
		// Error when the file does not exist
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(inputFile));
		} catch (FileNotFoundException e1) {
			throw new Exception ("The file does not exist");
		}

		String line;
		int lineNumber = 1;

		// checks the lines as long as there is something to read in the file
		try {
			while ((line = reader.readLine()) != null) {

				try {
					// put the content of the line in the Reference object
					Reference reference = mapLineToReference(line);
					// add the reference to a list of references
					references.add(reference);
				} catch (Exception exception) {
					// catch any exception during the mapping and create an FileError object
					FileError error = new FileErrorImpl(lineNumber, exception.getMessage(), line);
					// add the error to a list of FileError
					errors.add(error);
				}

				lineNumber++;
			}
		} catch (IOException e) {
			throw new Exception("Error while reading the txt file");
		}

		TemplateFile templateFile = new TemplateFileImpl(references, errors, inputFile.getName());
		return templateFile;
	}

	private Reference mapLineToReference(String line) throws Exception {

		Reference reference = new ReferenceImpl();

		// get the 4 parameters in the line and then put them in a list
		String[] lineToStringTab = StringUtils.split(line, ";");

		// error if the list does not have 4 parameters
		if (lineToStringTab.length != 4) {

			throw new Exception("Wrong number of parameters in the Reference");
		} else {
			// Mapping of the reference object
			setNumReference(reference, lineToStringTab[0]);
			setReferenceColor(reference, lineToStringTab[1]);
			setPrice(reference, lineToStringTab[2]);
			setSize(reference, lineToStringTab[3]);
		}

		return reference;
	}

	private void setNumReference(Reference reference, String numReference) throws Exception {

		// checks that reference is 10 digits
		if (numReference.length() == 10) {
			for (int i = 0; i < 10; i++) {
				if (!Character.isDigit(numReference.charAt(i))) {
					throw new Exception("Incorrect value for numReference");
				}
			}
			// set number value for reference
			reference.setNumber(numReference);

		} else {
			throw new Exception("Incorrect value for numReference");
		}

	}

	private void setSize(Reference reference, String size) throws Exception {

		try {
			// the String to int conversion happens here
			int sizeInt = Integer.parseInt(size.trim());
			reference.setSize(sizeInt);
		} catch (Exception exception) {
			throw new Exception("Incorrect value for size");
		}

	}

	private void setPrice(Reference reference, String price) throws Exception {

		BigDecimal priceBD = new BigDecimal(price);
		reference.setPrice(priceBD);

	}

	private void setReferenceColor(Reference reference, String color) throws Exception {
		// checks if color is R, G, B char
		if (color.equals("R") || color.equals("B") || color.equals("G")) {
			char colorChar = color.charAt(0);
			reference.setColor(colorChar);
		}

		else
			throw new Exception("Incorrect value for color");
	};

}
