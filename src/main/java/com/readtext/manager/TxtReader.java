package com.readtext.manager;

import java.io.File;
import com.readtext.model.TemplateFile;


public interface TxtReader {

	/**
	 * Method that read the txt file and put information in the TemplateFile Object
	 * @param inputFile
	 * @return TemplateFile
	 * @throws Exception 
	 */
	public TemplateFile readFile(File inputFile) throws Exception;

	
}
