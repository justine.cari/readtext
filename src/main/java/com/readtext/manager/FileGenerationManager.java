package com.readtext.manager;

import java.io.File;

import com.readtext.model.UserInputs;

public interface FileGenerationManager {
	/**
	 * Generate the JSON or XML file
	 * @param inputs : the user choises for the pathes and the type of file
	 * @return File
	 * @throws Exception 
	 */
	public File generateFile(UserInputs inputs) throws Exception;

}
