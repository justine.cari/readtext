package com.readtext.impl.model;

import java.util.List;

import com.readtext.model.FileError;
import com.readtext.model.Reference;
import com.readtext.model.TemplateFile;

public class TemplateFileImpl implements TemplateFile {
	
	private String inputFile;	
	
	private List<Reference> references;
	
	private List<FileError> errors;
	
	public TemplateFileImpl(List<Reference> references, List<FileError> errors, String inputFile) {
		
		this.references = references;
		this.errors = errors;
		this.inputFile = inputFile;
	}
	
	public String getInputFile() {
		return inputFile;
	}
	
	public List<Reference> getReferences() {
		return references;
	}
	
	public List<FileError> getErrors() {
		return errors;
	}
		
	
}
