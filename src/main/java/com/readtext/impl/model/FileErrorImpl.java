package com.readtext.impl.model;

import com.readtext.model.FileError;

public class FileErrorImpl implements FileError {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//indicates the place of the line in the file
	private int line;
	
	//the error message
	private String message;
	
	//the whole line of the txt file
	private String value;
	
	public FileErrorImpl(int line, String message, String value) {
	
		this.line = line;
		this.message = message;
		this.value = value;
	}
	
	public int getLine() {
		return line;
	}
	
	public String getMessage() {
		return message;
	}

	public String getValue() {
		return value;
	}
	
		

}
