package com.readtext.impl.model;

import java.io.File;

import com.readtext.model.UserInputs;

public class UserInputsImpl implements UserInputs {


	private File txtFile;
	private String type;
	private String generatedFilePath;
	
		
	public UserInputsImpl(File txtFile, String type, String generatedFilePath) {
		this.txtFile = txtFile;
		this.type = type;
		this.generatedFilePath = generatedFilePath;
	}


	public File getTxtFile() {
		return txtFile;
	}


	public String getType() {
		return type;
	}


	public String getGeneratedFilePath() {
		return generatedFilePath;
	}


	
	
	
	
	
}
