package com.readtext.impl.model;

import java.math.BigDecimal;

import com.readtext.model.Reference;

public class ReferenceImpl implements Reference {

	
	private String number;
	private char color;
	private BigDecimal price;
	private int size;
	
	public String getNumber() {
		return number;
	}
	
	public void setNumber(String number) {
		this.number = number;
	}
	
	public char getColor() {
		return color;
	}
	
	public void setColor(char color) {
		this.color = color;
	}
	
	public BigDecimal getPrice() {
		return price;
	}
	
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	
	public int getSize() {
		return size;
	}
	
	public void setSize(int size) {
		this.size = size;
	}
	
}
