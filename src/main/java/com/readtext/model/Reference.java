package com.readtext.model;

import java.math.BigDecimal;


public interface Reference {

	/**
	 * the number of the reference
	 * @return String number
	 */
	public String getNumber();
	
	/**
	 * sets the number of the reference
	 * @param number
	 */
	public void setNumber(String number);
	
	/**
	 * the color of the reference
	 * @return char color
	 */
	public char getColor() ;
	
	/**
	 * sets the color of the reference
	 * @param color
	 */
	public void setColor(char color);
	
	/**
	 * the price of the reference
	 * @return BigDecimal price
	 */
	public BigDecimal getPrice();
	
	/**
	 * sets the price of the reference
	 * @param price
	 */
	public void setPrice(BigDecimal price);
	
	/**
	 * the size of the reference
	 * @return int size
	 */
	public int getSize();
	
	/**
	 * sets the size of the reference
	 * @param size
	 */
	public void setSize(int size);
	
}
