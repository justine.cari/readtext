package com.readtext.model;

import java.io.File;

public interface UserInputs {

	/**
	 * get the File object for the inputFile
	 * @return txtFile
	 */
	public File getTxtFile();

	

	/**
	 * get the type of the file to create
	 * @return type
	 */
	public String getType();

	

	/**
	 * get the path for the file to create
	 * @return generatedFilePath
	 */
	public String getGeneratedFilePath();

	

}
