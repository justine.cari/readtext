package com.readtext.model;

public interface FileError{

	/**
	 * get the position of line in txt file
	 * @return int Line
	 */
	public int getLine();

	

	/**
	 * Get the error message
	 * @return String message
	 */
	public String getMessage();

	

	/**
	 * get the value of the line  
	 * @return String value
	 */
	public String getValue();


}
