package com.readtext.model;

import java.util.List;

public interface TemplateFile {

	/**
	 * The list of references in the file
	 * @return references
	 */
	public List<Reference> getReferences();
	
	 
	
	/**
	 *  The list of errors in the file
	 * @return List<FileError> errors()
	 */
	public List<FileError> getErrors(); 
	
	
	
	/**
	 * The name of the inputFile
	 * @return inputFile
	 */
	public String getInputFile();
	
	
}
